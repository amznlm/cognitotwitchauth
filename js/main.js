(function(){
  'use strict';
  // need for the connect button
    var tInit = Twitch.init({clientId: 'iy6rv9v4uku6nht6h0koijy4i6c6qxd'}, function(error, status) {
    console.log("twitch status:" + JSON.stringify(status));
    if (status.authenticated) {
      // we're logged in :)
      $('.status input').val('Logged in! Allowed scope: ' + status.scope);
      // Show the data for logged-in users
      $('.authenticated').removeClass('hidden');

      var getOpenIdTokenForTwitchUser = function(tId,callback){
        //Initialize CognitoIdentity service object with IAM user which have permissions to CognitoIdentity services
        var cognitoId = new AWS.CognitoIdentity({accessKeyId:'AKIAJA3LRTBZPRMDXRXQ',
          secretAccessKey:'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',
          region:'us-west-2'});
        var identityPoolId = 'us-west-2:0e01cd40-9a03-4299-b0a7-2fa65571c052';

        // cognitoId.getCredentialsForIdentity({IdentityId:'us-west-2:426aaf80-473e-443d-84f6-eb255a2a1419'},function(err,data){
        //     if(err){
        //         console.log('err: getCredentialsForIdentity:' + err);
        //     }else{
        //         console.log('data:getCredentialsForIdentity:' + JSON.stringify(data));
        //     }
        // });
        cognitoId.getOpenIdTokenForDeveloperIdentity({
            IdentityPoolId: identityPoolId,
            Logins: {
                'login.lm.poc': tId
            }
          },function(err,data){
            if(err){
                console.log('err:getOpenIdTokenForDeveloperIdentity:' + err);
            }else{
                console.log('data:getOpenIdTokenForDeveloperIdentity:' + JSON.stringify(data));
            }
            callback && callback(err,data);
        });
      };

      var authenticateTwitchUserInCognito = function(data,callback){
        var identityPoolId = 'us-west-2:0e01cd40-9a03-4299-b0a7-2fa65571c052';
        // Initialize the Amazon Cognito credentials provider
        AWS.config.region='us-west-2';
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: identityPoolId,
            IdentityId:data.IdentityId,
            Logins: {
              'cognito-identity.amazonaws.com':data.Token
            }
        });

        AWS.config.credentials.get(function(){

          var accessKeyId = AWS.config.credentials.accessKeyId;
          var secretAccessKey = AWS.config.credentials.secretAccessKey;
          var sessionToken = AWS.config.credentials.sessionToken;
          var identityId = AWS.config.credentials.identityId;
          console.log('Twitch user credentails in aws|accessKeyId:' + accessKeyId);
          console.log('secretAccessKey:' + secretAccessKey);
          console.log('sessionToken:' + sessionToken);
          console.log('identityId:' + identityId);
        });
      };

      Twitch.api({method: 'user'}, function(error, user) {
        console.log('user:' + JSON.stringify(user));
        console.log('token:' + Twitch.getToken());
        console.log('name:' + user.name);
        getOpenIdTokenForTwitchUser(user.name,function(err,data){
          if(err){
              console.log('err:getOpenIdTokenForTwitchUser:' + err);
          }else{
              //console.log('data:getOpenIdTokenForTwitchUser:' + JSON.stringify(data));
              authenticateTwitchUserInCognito(data);
          }
        });
      });

    } else {
      $('.status input').val('Not Logged in! Better connect with Twitch!');
      // Show the twitch connect button
      $('.authenticate').removeClass('hidden');
    }
  });

$('.twitch-connect').click(function() {
  Twitch.login({
    redirect_uri:'http://localhost/lumbermillpoc',
    scope: ['user_read', 'channel_read']
  });
})

$('#logout button').click(function() {
  Twitch.logout();

  // Reload page and reset url hash. You shouldn't
  // need to do this.
  window.location = window.location.pathname
})

$('#get-name button').click(function() {
  Twitch.api({method: 'user'}, function(error, user) {
    $('#get-name input').val(user.display_name);
  });
})

$('#get-stream-key button').click(function() {
  Twitch.api({method: 'channel'}, function(error, channel) {
    $('#get-stream-key input').val(channel.stream_key);
  });
})

}());

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail());
  console.log('googleUser:' + JSON.stringify(googleUser));
  console.log('profile:' + JSON.stringify(profile));

  var authResponse = googleUser.getAuthResponse();
  googleToken = authResponse['id_token'];
  console.log('authResponse:' + JSON.stringify(authResponse));
  console.log('id_token:' + authResponse['id_token']);

  // Set the region where your identity pool exists (us-east-1, eu-west-1)
  AWS.config.region = 'us-west-2';

  // Add the Google access token to the Cognito credentials login map.
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
     IdentityPoolId: 'us-west-2:ed27747c-842c-4a21-8e7a-bba2aa649d57',
     Logins: {
        'accounts.google.com': authResponse['id_token']
     }
  });

  // Obtain AWS credentials
  AWS.config.credentials.get(function(){
     // Access AWS resources here.
     // Credentials will be available when this function is called.
     var accessKeyId = AWS.config.credentials.accessKeyId;
     var secretAccessKey = AWS.config.credentials.secretAccessKey;
     var sessionToken = AWS.config.credentials.sessionToken;
     var identityId = AWS.config.credentials.identityId;

     console.log(' google user credentails in aws| accessKeyId:' + accessKeyId);
     console.log(' secretAccessKey:' + secretAccessKey);
     console.log(' sessionToken:' + sessionToken);
     console.log(' identityId:' + identityId);

  });
}
